import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatSelectModule} from '@angular/material/select';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatButtonModule, MatChipsModule, MatIconModule, MatInputModule, MatTooltipModule } from '@angular/material';
import { NgModule, NO_ERRORS_SCHEMA} from '@angular/core';
import { AddTaskComponent } from './forms/add-task/add-task.component';
import { AddUserComponent } from './forms/add-user/add-user.component';
import {RouterModule} from '@angular/router';
import {AppComponent} from './app.component';


@NgModule({
    declarations: [
      AddTaskComponent,
      AddUserComponent,
      AppComponent,
    ],
    imports: [
      BrowserModule,
      BrowserAnimationsModule,
      MatSelectModule,
      MatInputModule,
      MatChipsModule,
      MatIconModule,
      MatButtonModule,
      MatCheckboxModule,
      MatTooltipModule,
      RouterModule.forRoot([
          {
              path: 'add-user',
              component: AddUserComponent
          },
          {
              path: 'add-task',
              component: AddTaskComponent
          }
      ])
    ],
    providers: [],
    bootstrap: [AppComponent],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class AppModule { }
