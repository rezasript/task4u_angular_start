import {
    Component,
    ElementRef,
    OnInit,
    QueryList,
    ViewChild,
    ViewContainerRef,
    AfterViewInit,
    ViewChildren,
    ChangeDetectorRef
} from '@angular/core';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.sass']
})

export class AddUserComponent implements OnInit, AfterViewInit {
    @ViewChild('emailTmpl') emailTmpl;
    @ViewChild('furtherEmails', {read: ViewContainerRef}) furtherEmails;
    @ViewChildren('email') email: QueryList<ElementRef>;

    public clone(): void {
        this.furtherEmails.createEmbeddedView(this.emailTmpl);
        this.cd.detectChanges();
    }

    constructor(private cd: ChangeDetectorRef) { }

    ngOnInit() {
        this.clone();
    }

    ngAfterViewInit() {
        this.email.changes.subscribe(() => {
            this.email.last.nativeElement.focus();
        });
    }
}
