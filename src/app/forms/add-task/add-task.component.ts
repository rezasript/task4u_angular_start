import { Component } from '@angular/core';
import {ENTER, COMMA, SPACE, TAB} from '@angular/cdk/keycodes';
import {MatChipInputEvent, TooltipPosition} from '@angular/material';


@Component({
    selector: 'app-add-task',
    templateUrl: './add-task.component.html',
    styleUrls: ['./add-task.component.sass']
})
export class AddTaskComponent {
    projects = ['Dummy Project', 'Important Project', 'Local Project'];
    selectable = true;
    removable = true;
    addOnBlur = true;

// Enter, comma
    separatorKeysCodes = [ENTER, COMMA, SPACE, TAB];

    technologies = [
        { name: 'HTML' },
        { name: 'CSS' },
        { name: 'JavaScript' },
        { name: 'PHP' },
    ];

    // Add technologies
    add(event: MatChipInputEvent): void {
        const input = event.input;
        const value = event.value;

        if ((value || '').trim()) {
            this.technologies.push({ name: value.trim() });
        }

        // Reset the input value
        if (input) {
            input.value = '';
        }
    }

    // Remove technology
    remove(tech: any): void {
        const index = this.technologies.indexOf(tech);

        if (index >= 0) {
            this.technologies.splice(index, 1);
        }
    }
}
